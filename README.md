## Dockerized Django ##

Spin up the Django environment with:
> make up  
or  
> docker-compose up -d

Access Django admin in your browser at http://localhost:5555/admin

Access the front-end at http://localhost:5555