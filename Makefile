build:
	docker-compose build

up:
	docker-compose up -d

up-non-daemon:
	docker-compose up

start:
	docker-compose start

stop:
	docker-compose stop

down:
	docker-compose down

restart:
	docker-compose stop && docker-compose start

restart-server:
	docker-compose stop server && docker-compose start server

shell-server:
	docker exec -ti server bash

shell-db:
	docker exec -ti postgres bash

log-server:
	docker-compose logs server

log-db:
	docker-compose logs database

collectstatic:
	docker exec server /bin/sh -c "python manage.py collectstatic --noinput"

migrations:
	docker exec server /bin/sh -c "python manage.py makemigrations; python manage.py migrate"
